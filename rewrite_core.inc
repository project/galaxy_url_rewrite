<?php

function custom_url_rewrite_outbound(&$pl_path, &$options, $original_path) {
  if (variable_get('galaxy_url_rewrite_enabled', FALSE)) {
    $p_args = explode('/', $pl_path);
    $res = @db_fetch_array(db_query("
      SELECT dst, src
      FROM {gal_url_rewrite_alias}
      WHERE '%s' LIKE src AND _src_args_num = %d AND disabled = 0
      LIMIT 1", $pl_path, count($p_args)));
    list($dst, $src) = array($res['dst'], $res['src']);

    if ($dst && $src) {
      if (strpos($src, '%') !== false) {
        $dynamic_args = array();
        $s_args = explode('/', $src);
        $d_args = explode('/', $dst);
        if (count($s_args) == count($p_args)) {
       /* example: iterate the array $p_args = array('university', 'struct', 'chairs', '0000000000000000')
          and extract dynamic arguments $dynamic_args, used template
          $s_args = array('university', 'struct', 'chairs', '%'), which stored in database;
          in this example '%' is equal '0000000000000000' */
          foreach ($s_args as $index => $arg) {
            if ($arg == '%') {
              $dynamic_args[]= $p_args[$index];
            }
          }
       /* iterate the array $d_args = array('chairs', '%') and change dynamic
          arguments by real values from $dynamic_args */
          foreach ($d_args as $index => $arg) {
            if ($arg == '%') {
              $d_args[$index]= array_shift($dynamic_args);
            }
          }
          $pl_path = implode('/', $d_args);
        }
      } else {
        $pl_path = $dst;
      }
    }
  }
}


function custom_url_rewrite_inbound(&$pl_path, $path, $path_language) {
  if (variable_get('galaxy_url_rewrite_enabled', FALSE)) {
    $p_args = explode('/', $pl_path);
    $res = @db_fetch_array(db_query("
      SELECT dst, src
      FROM {gal_url_rewrite_alias}
      WHERE '%s' LIKE dst AND _dst_args_num = %d AND disabled = 0
      LIMIT 1", $pl_path, count($p_args)));
    list($dst, $src) = array($res['dst'], $res['src']);

    if ($dst && $src) {
      if (strpos($src, '%') !== false) {
        $dynamic_args = array();
        $s_args = explode('/', $src);
        $d_args = explode('/', $dst);
        if (count($d_args) == count($p_args)) {
          foreach ($d_args as $index => $arg) {
            if ($arg == '%') {
              $dynamic_args[]= $p_args[$index];
            }
          }
          foreach ($s_args as $index => $arg) {
            if ($arg == '%') {
              $s_args[$index]= array_shift($dynamic_args);
            }
          }
          $pl_path = implode('/', $s_args);
        }
      } else {
        $pl_path = $src;    
      }
    }
  }
}

