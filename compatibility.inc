<?php

  $GLOBALS['URL_REWRITE_ACCESS_ARGUMENTS'] = array('administer url_rewrite');
  $GLOBALS['URL_REWRITE_ROOT_PATH_DYNAMIC'] = 'admin/build/url_rewrite';


function galaxy_get_form_selected_items($item_set, $prefix = '') {
  $selected_items = array();
  foreach ($item_set as $item_key => $item_value) {
    if ($item_value) $selected_items[] = $prefix.$item_key;
  }
  return $selected_items;
}

function get_chmod($file){return substr(sprintf('%o', fileperms($file)), -4);}